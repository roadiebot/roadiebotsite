module.exports = function(application){
	application.get('/contact', function(req, res){
		application.app.controllers.contact.contactForm(application, req, res);
	});

////Chama o módulo responsável por salvar a "notícia", que no caso será nosso
//formulário para contato.

//se não der certo tenta com post, mudar também no html
	application.post('/formsave', function(req, res){
		application.app.controllers.contact.formSave(application, req, res);
	});

}